//
//  ViewController.swift
//  Pioneras
//
//  Created by Master Móviles on 10/10/16.
//  Copyright © 2016 Master Móviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detailVC = segue.destination as! DetailViewController
        detailVC.filename = segue.identifier
    }

    @IBAction func retornoDeSegueSecundaria(segue: UIStoryboardSegue) {
        print("Retorno")
    }
}

